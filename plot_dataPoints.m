function [ ] = plot_dataPoints( dataPoints, img )

a = 5;
[m,~] = size(dataPoints);
[height,~,~] = size(img);

dataPoints = round(dataPoints);

imshow(img);
hold on;

LEFT = dataPoints(:,[2 3]);
LEFT(:,2) = height - LEFT(:,2);

RIGHT = dataPoints(:,[7 8]);
RIGHT(:,2) = height - RIGHT(:,2);

scatter(LEFT(:,1),LEFT(:,2),a,'red','filled');
scatter(RIGHT(:,1),RIGHT(:,2),a,'green','filled');
end

