function [result] = load_tsv(fileName)

result = tdfread(fileName);
result = struct2array(result);

end