%% first exercise for the eye movement class

clear;
close all;

%% exercise 1
close all;
clc

samplesFile = 'samples.data';
imageFile = 'stimuli.jpg';

tsv_data = load_tsv(samplesFile);
parsed_tsv_data = remove_invalid(tsv_data);
image = load_image(imageFile);
plot_dataPoints(parsed_tsv_data, image);

%% exercise 2
close all;
clc
dis

tsv_data = load_tsv(samplesFile);
parsed_tsv_data = remove_invalid(tsv_data);
fixations = I-DT(parsed_tsv_data, 
image = load_image(imageFile);
plot_dataPoints(