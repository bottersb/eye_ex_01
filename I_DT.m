function [ fixations ] = I_DT( protocol, dist, durt )
%I-DT Summary of this function goes here
%   Detailed explanation goes here
fix;
finfix;
t = 1;
idx=1;
[height,width]= size(protocol);
i = 1;
while i < height
    if(protocol(i+1,1)-protocol(i,1)>durt)
        fix(idx)=protocol(i);
        fix(idx+1)=protocol(i+1);
        idx= idx+2;
        windowd = fix((max(fix(:,2))-min(fix(:,2)))+(max(fix(:,3))-min(fix(:,3))));
        i = i+1;
        while windowd <= dist
            fix(idx) =protocol(i);
            windowd = fix((max(fix(:,2))-min(fix(:,2)))+(max(fix(:,3))-min(fix(:,3))));
            i= i+1;
        end
        
        finfix(t)=((max(fix(:,2))+min(fix(:,2)))/2+(max(fix(:,3))+min(fix(:,3)))/2);
        t=t+1;
        fix = [];
    else
        fix(1)=fix(2);
        fix(2) = [];
        idx=2;
    end
    i=i+1;
end

fixations=finfix;

end

